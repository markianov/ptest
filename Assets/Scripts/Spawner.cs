﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject player;
    public GameObject[] zombie;
    public GameObject mummy;

    public GameObject[] coins;

    private MapManager map;

    void Start() {
        map = MapManager.manager;
        playerSpawn();
        StartCoroutine("coinSpawn");
    }

    private void playerSpawn() {
        player.SetActive(true);
        player.transform.position = getRandSpawnZone();
    }

    public void zombieSpawn() {
        if (zombie[0].activeInHierarchy) {
            zombie[1].SetActive(true);
            zombie[1].transform.position = getRandSpawnZone();
        }
        else {
            zombie[0].SetActive(true);
            zombie[0].transform.position = getRandSpawnZone();
        }
    }

    public void mummySpawn() {
        mummy.SetActive(true);
        mummy.transform.position = getRandSpawnZone();
    }

    IEnumerator coinSpawn() {
        while (true) {
            for (int i = 0; i < coins.Length; i++) {
                if (!coins[i].activeInHierarchy) {
                    coins[i].SetActive(true);
                    coins[i].transform.position = getRandSpawnZone();
                    break;
                }
            }
            yield return new WaitForSeconds(5);
        }
    }
     
    private Vector2 getRandSpawnZone() {
        int counter = Random.Range(0, map.getGroundCount());

        for (int x = 0; x < map.getWidth(); x++) {
            for (int y = 0; y < map.getHeight(); y++) {
                if (map.getBlock(x, y)) {
                    if (counter <= 0) {
                        return new Vector2(x, y);
                    }
                    else {
                        counter--;
                    }
                }
            }
        }
        return Vector2.zero;
    }
}
