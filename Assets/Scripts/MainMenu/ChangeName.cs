﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeName : MonoBehaviour {
    
    public InputField nameInput;

    void Start() {
        nameInput.text = GameManager.getGameManager().userName;
    }

    public void updateName() {
        if (nameInput.text == "") {
            GameManager.getGameManager().userName = "Player";
        }
        else {
            GameManager.getGameManager().userName = nameInput.text;
        }

        gameObject.SetActive(false);
    }
}
