﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMenu : MonoBehaviour {

    public GameObject changeNamePanel;
    public GameObject scorePanel;

    public void startGame() {
        SceneManager.LoadScene(1);
    }
    
    public void showScore() {
        scorePanel.SetActive(true);
    }

    public void changeName() {
        changeNamePanel.SetActive(true);
    }

    public void exitToMenu() {
        scorePanel.SetActive(false);
        changeNamePanel.SetActive(false);
    }
}
