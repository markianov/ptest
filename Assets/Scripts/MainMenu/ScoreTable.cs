﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ScoreTable : MonoBehaviour {

    public RectTransform content;
    public GameObject tableItem;
    
    void Start() {
        try {
            buildTable();
        }
        catch(NullReferenceException ex) {
            Debug.Log("Data not found!");
        }
    }

    public void buildTable() {
        UserList userList = SaveToXML.getXML().statistics;

        float yCoord = -30;
        float coordStep = 55;
        userList.sort();

        foreach (User user in userList.list) {
            Transform panel = Instantiate(tableItem, content, false).transform;
            RectTransform rt = panel.GetComponent<RectTransform>();
            rt.anchoredPosition = new Vector2(rt.anchoredPosition.x, yCoord);
            
            yCoord -= coordStep;

            panel.FindChild("UserName").GetComponent<Text>().text = user.name;
            panel.FindChild("UserScore").GetComponent<Text>().text = user.score.ToString();
            panel.FindChild("CauseOfDeth").GetComponent<Text>().text = user.causeOfDeath;
            panel.FindChild("TimeInGame").GetComponent<Text>().text = user.gameTime + "s";
            panel.FindChild("Date").GetComponent<Text>().text = user.date.ToShortDateString();

            content.offsetMax = new Vector2(content.offsetMax.x, content.offsetMax.y + coordStep);
        }
    }
}
