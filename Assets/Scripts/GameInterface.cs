﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameInterface : MonoBehaviour {

    public Text coinsCount;
    public GameObject gameOverPanel;
    public Text gameOverScore;

    public static GameInterface gi;

    void Start() {
        gi = this;
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            GameManager.getGameManager().gameOver(CauseOfDeath.Quit);
        }
    }

    public void updateInfo() {
        coinsCount.text = GameManager.getGameManager().getScore().ToString();
    }

    public void backToMenu() {
        SceneManager.LoadScene(0);
    }
    public void restart() {
        SceneManager.LoadScene(1);
    }
}
