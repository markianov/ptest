﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour {

    private float startTime;
	
	void Start () {
        startTime = Time.time;
	}
	
	
	void Update () {
        GameManager.getGameManager().timeInGame = Time.time - startTime;
	}
}
