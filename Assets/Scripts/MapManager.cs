﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour {

    public GameObject ground;
    public GameObject wall;

    private bool[,] map;
    private int groundCount;

    public static MapManager manager;

    void Awake() {
        generateMap(40, 40, 4, 50);
    }

    void Start() {
        manager = this;
    }

    public bool getBlock(int x, int y) {
        if (x < 0 || y < 0 || x >= map.GetLength(0) || y >= map.GetLength(1)){
            return false;
        }
        return map[x, y];
    }

    public int getWidth() {
        return map.GetLength(0);
    }
    public int getHeight() {
        return map.GetLength(1);
    }
    public int getGroundCount() {
        return groundCount;
    }

    private void generateMap(int width, int height, int roomSize, int wallPerCent) {
        map = new bool[width, height];

        bool[,] roomMap = new bool[width / roomSize, height / roomSize];

        for (int i = 0; i < roomMap.GetLength(0); i++) {
            for (int j = 0; j < roomMap.GetLength(1); j++) {
                roomMap[i, j] = Random.value < 0.5f;
            }
        }

        int percent = analyseWallPerCent(roomMap);

        int counter = 0;
        while (percent != wallPerCent && counter < 1000) {
            bool less = percent < wallPerCent;
            counter++;
            for (int i = Random.Range(0, roomMap.GetLength(0)); i < roomMap.GetLength(0); i++) {
                for (int j = Random.Range(0, roomMap.GetLength(1)); j < roomMap.GetLength(1); j++) {
                    if (roomMap[i, j] == less) {
                        roomMap[i, j] = !less;
                        i = roomMap.GetLength(0);
                        j = roomMap.GetLength(1);
                    }
                }
            }

            percent = analyseWallPerCent(roomMap);
        }

        var rooms = new List<Room>();

        for (int i = 0; i < roomMap.GetLength(0); i++) {
            for (int j = 0; j < roomMap.GetLength(1); j++) {
                if (roomMap[i, j]) {
                    Room room = new Room();

                    bool[,] r = generateRoom(4);
                    for (int x = 0; x < roomSize; x++) {
                        for (int y = 0; y < roomSize; y++) {
                            map[i * roomSize + x, j * roomSize + y] = r[x, y];
                            if (r[x, y]) {
                                room.points.Add(new Point(i * roomSize + x, j * roomSize + y));
                            }
                        }
                    }

                    rooms.Add(room);
                }
            }
        }

        connectRooms(rooms);

        buildMap(map);
    }

    private void connectRooms(List<Room> rooms) {
        for (int i = 0; i < rooms.Count; i++) {
            Point p1 = rooms[i].points[0];

            var nearestRooms = new List<Room>();

            foreach (Room r in rooms) {
                if (r != rooms[i]) {
                    int dis = Room.distance(rooms[i], r);
                    if (nearestRooms.Count == 0) {
                        nearestRooms.Add(r);
                    }
                    else if (dis < Room.distance(rooms[i], nearestRooms[0])) {
                        nearestRooms[0] = r;
                    }
                    else if (nearestRooms.Count == 1) {
                        nearestRooms.Add(r);
                    }
                    else if (dis < Room.distance(rooms[i], nearestRooms[1])) {
                        nearestRooms[1] = r;
                    }
                    else if (nearestRooms.Count == 2) {
                        nearestRooms.Add(r);
                    }
                    else if (dis < Room.distance(rooms[i], nearestRooms[2])) {
                        nearestRooms[2] = r;
                    }
                }
            }

            for (int j = 0; j < nearestRooms.Count; j++) {
                Point p2 = nearestRooms[j].points[0];
                while (p1.x != p2.x || p1.y != p2.y) {
                    if (p1.x < p2.x) {
                        map[p1.x + 1, p1.y] = true;
                        p1.x = p1.x + 1;
                    }
                    else if (p1.x > p2.x) {
                        map[p1.x - 1, p1.y] = true;
                        p1.x = p1.x - 1;
                    }
                    if (p1.y < p2.y) {
                        map[p1.x, p1.y + 1] = true;
                        p1.y = p1.y + 1;
                    }
                    else if (p1.y > p2.y) {
                        map[p1.x, p1.y - 1] = true;
                        p1.y = p1.y - 1;
                    }
                }
            }
        }
    }

    private bool[,] generateRoom(int roomSize) {
        bool[,] room = new bool[roomSize, roomSize];
        int blockSize = roomSize / 2; ;
        for (int i = 0; i < 3; i++) {
            int pointX = Random.Range(0, roomSize - blockSize);
            int pointY = Random.Range(0, roomSize - blockSize);
            for (int j = pointX; j < pointX + blockSize; j++) {
                for (int k = pointY; k < pointY + blockSize; k++) {
                    room[j, k] = true;
                }
            }
        }

        return room;
    }

    private int analyseWallPerCent(bool[,] m) {
        float percent = 0;
        for (int i = 0; i < m.GetLength(0); i++) {
            for (int j = 0; j < m.GetLength(1); j++) {
                if (!m[i, j]) percent += 1f;
            }
        }
        
        return (int)(percent / ((m.GetLength(0) * m.GetLength(1)) / 100f));
    }

    //Метод BuildMap - рисует карту используя картинки земли и стен.
    private void buildMap(bool[,] m) {
        for (int i = 0; i < m.GetLength(0); i++) {
            for (int j = 0; j < m.GetLength(1); j++) {
                if (i == 0) {
                    Instantiate(wall, new Vector3(i - 1, j, 0), wall.transform.rotation);
                }
                else if (i == m.GetLength(0) - 1) {
                    Instantiate(wall, new Vector3(i + 1, j, 0), wall.transform.rotation);
                }
                if (j == 0) {
                    Instantiate(wall, new Vector3(i, j - 1, 0), wall.transform.rotation);
                }
                else if (j == m.GetLength(1) - 1) {
                    Instantiate(wall, new Vector3(i, j + 1, 0), wall.transform.rotation);
                }

                if (!m[i, j]) {
                    Instantiate(wall, new Vector3(i, j, 0), wall.transform.rotation);
                }
                else {
                    Instantiate(ground, new Vector3(i, j, 0), ground.transform.rotation);
                    groundCount++;
                }
            }
        }
    }

    class Room {
        public List<Point> points;
        public Room() {
            points = new List<Point>();
        }

        public static int distance(Room r1, Room r2) {
            Point p1 = r1.points[0];
            Point p2 = r2.points[1];

            return Mathf.Abs(p1.x - p2.x) + Mathf.Abs(p1.y - p2.y);
        }
    }

    class Point {
        public int x;
        public int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
}
