﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager {

    public float timeInGame;
    public string userName = "Player";
    public CauseOfDeath causeOfDeath;

    private int score;
    
    private static GameManager gameManager;

    public static GameManager getGameManager() {
        if (gameManager == null) {
            gameManager = new GameManager();
        }

        return gameManager;
    }
    
    public void addCoin() {
        score++;
    }

    public int getScore() {
        return score;
    }

    public void clearScore() {
        score = 0;
    }

    public void gameOver(CauseOfDeath cod) {
        causeOfDeath = cod;

        if(cod == CauseOfDeath.KilledByMummy) {
            clearScore();
        }

        SaveToXML.getXML().save();

        int s = score;

        clearScore();

        if (cod != CauseOfDeath.Quit) {
            GameInterface.gi.gameOverPanel.SetActive(true);
            GameInterface.gi.gameOverScore.text = "Your score is " + s;
        }
        else {
            SceneManager.LoadScene(0);
        }
    }
}

public enum CauseOfDeath {
    KilledByZombie,
    KilledByMummy,
    Quit
}
