﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System;

public class SaveToXML {

    private static SaveToXML xml;

    private SaveToXML() {
        try {
            load();
        }
        catch (FileNotFoundException ex) {
            Debug.Log("XML file not found!");
            statistics = new UserList();
        }
    }

    public static SaveToXML getXML() {
        if(xml == null) {
            xml = new SaveToXML();
        }
        return xml;
    }

    public UserList statistics;

    public void save() {
        GameManager gm = GameManager.getGameManager();
        User user = new User() { name = gm.userName, causeOfDeath = gm.causeOfDeath.ToString(), date = DateTime.Now, gameTime = (int)gm.timeInGame, score = gm.getScore() };

        statistics.list.Add(user);

        XmlSerializer serializer = new XmlSerializer(typeof(UserList));
        FileStream stream = new FileStream(Application.dataPath + "/user_statistics.xml", FileMode.Create);
        serializer.Serialize(stream, statistics);
        stream.Close();
    }

    public void load() {
        XmlSerializer serializer = new XmlSerializer(typeof(UserList));
        FileStream stream = new FileStream(Application.dataPath + "/user_statistics.xml", FileMode.Open);
        statistics = serializer.Deserialize(stream) as UserList;
        stream.Close();
    }
}

[System.Serializable]
public class User : IEquatable<User>, IComparable<User> {
    [XmlElement("Name")]
    public string name;
    [XmlElement("Score")]
    public int score;
    [XmlElement("CauseOfDeath")]
    public string causeOfDeath;
    [XmlElement("TimeInGame")]
    public int gameTime;
    [XmlElement("Date")]
    public DateTime date;

    public bool Equals(User user) {
        if (user == null) return false;
        return date == user.date;
    }

    public override int GetHashCode() {
        return date.GetHashCode();
    }

    public int CompareTo(User user) {
        if (user == null)
            return 1;

        else
            return date.CompareTo(user.date);
    }
}

[System.Serializable]
public class UserList {
    [XmlArray("UserList")]
    public List<User> list = new List<User>();

    public void sort() {
        list.Sort();
        list.Reverse();
    }
}
