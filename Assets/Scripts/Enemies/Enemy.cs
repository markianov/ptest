﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
Класс Enemy - управялет поведением бота (зомби и мумии).
Единственное отличие этих персонажей, в том, что мумия отберает все монетки при убийстве игрока и двигается быстрее.

У ботов есть два режима поведения: случайное перемещение по карте и приследование игрока.
При случайном перемещении выбирается случайная точка на карте и от бота до неё строится маршрут с помощью алгоритма A Star, который находится в классе PathFinder.
При приследовании игрока путь строится также при помощи алгоритма A Star. Только маршрут обновляется каждый шаг.
*/
public class Enemy : MonoBehaviour {

    public float speed;
    public Vector3 moveTarget;

    public bool coinDanger; //Если true - при убийстве персонажа отбирает все монетки
    private bool hunter; //Если true - начинает охоту за игроком. Меняется режим поведения

    private Transform player;
    private Transform enemy;

    private bool lookRight = false;
    private SpriteRenderer sprite;

    private Animator animator;
    private bool stop = false;

    PathFinder pf;

    private PathFinder.Node path; //Конец маршрута. Это односвязный список. Каждый элемент знает толкьо своего родителя.

    private MapManager map;

    void Start() {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        pf = new PathFinder();
        enemy = transform;
        sprite = GetComponentInChildren<SpriteRenderer>();
        animator = GetComponentInChildren<Animator>();

        map = MapManager.manager;
    }

    void Update() {
        if (hunter) {
            huntPlayer();
        }
        else {
            randomWalk();
        }
    }

    public void startHunt() {
        hunter = true;
        path = null;
    }

    private void randomWalk() {
        if (moveTarget != Vector3.zero && Vector3.Distance(moveTarget, enemy.position) > 0.1f) {
            moveToTarget();
        }
        else if (path != null && path.GetParent() != null) {
            moveTarget = new Vector3(path.GetParent().x+0.1f, path.GetParent().y+0.1f, 0);
            path = path.GetParent();
        }
        else {
            for (int i = Random.Range(0, map.getWidth()); i < map.getWidth(); i++) {
                for (int j = Random.Range(0, map.getHeight()); j < map.getHeight(); j++) {
                    if (map.getBlock(i, j)) {
                        path = pf.FindPath(i, j, (int)enemy.position.x, (int)enemy.position.y);
                        if (path != null) {
                            moveTarget = new Vector3(path.x+0.1f, path.y+0.1f, 0);
                        }
                        return;
                    }
                }
            }
        }
    }

    private void huntPlayer() {
        if(Vector3.Distance(player.position, enemy.position) < 1) {
            moveTarget = player.position;
            moveToTarget();
        }
        else if (moveTarget != Vector3.zero && Vector3.Distance(moveTarget, enemy.position) > 0.1f) {
            moveToTarget();
        }
        else {
            path = pf.FindPath((int)player.position.x, (int)player.position.y, (int)enemy.position.x, (int)enemy.position.y);
            if (path != null) {
                moveTarget = new Vector3(path.GetParent().x+0.1f, path.GetParent().y+0.1f, 0);
            }
        }
    }

    private void moveToTarget() {
        if (!stop) {
            enemy.Translate((moveTarget - enemy.position).normalized * speed * Time.deltaTime);
            if (moveTarget.x < enemy.position.x && lookRight) {
                sprite.flipX = !sprite.flipX;
                lookRight = !lookRight;
            }
            else if (moveTarget.x > enemy.position.x && !lookRight) {
                sprite.flipX = !sprite.flipX;
                lookRight = !lookRight;
            }
        }
    }

    //Если игрок касается противника, тот начинает проигрывать анимацию атаки.
    void OnCollisionEnter2D (Collision2D col) {
        if (col.collider.tag == "Player") {
            animator.SetTrigger("Attack");
            stop = true;
        }
    }

    //Данный метод вызывается событием, активированным во время анимации. Если игрок няходится достоточно близко, атака убивает игрока.
    public void Attack() {
        if(Vector2.Distance(enemy.position, player.position) < 0.3f) {
            if (coinDanger) {
                GameManager.getGameManager().gameOver(CauseOfDeath.KilledByMummy);
            }
            else {
                GameManager.getGameManager().gameOver(CauseOfDeath.KilledByZombie);
            }
        }
        else {
            stop = false;
        }
    }
}
