﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Алгоритм поиска пути - A Star.
public class PathFinder {

    private List<Node> openList;
    private List<Node> closedList;

    //bool[,] map;
    MapManager map;

    public PathFinder() {
        openList = new List<Node>();
        closedList = new List<Node>();
        map = MapManager.manager;
    }

    public Node FindPath(int xStart, int yStart, int xFinish, int yFinish) {
        openList.Clear();
        closedList.Clear();

        openList.Add(new Node(xStart, yStart, 0));
        Node node;
        var finishNode = new Node(xFinish, yFinish, 0);

        while (openList.Count != 0) {
            Node min = null;

            foreach (Node n in openList) {
                if (min == null || min.f > n.f) {
                    min = n;
                }
            }
            node = min;

            openList.Remove(node);
            closedList.Add(node);

            try {
                if (map.getBlock(node.x + 1, node.y)) {
                    var n = new Node(node.x + 1, node.y, distance(xFinish, node.x + 1, yFinish, node.y));
                    setNodeParent(n, node);
                }
                if (map.getBlock(node.x, node.y + 1)) {
                    var n = new Node(node.x, node.y + 1, distance(xFinish, node.x, yFinish, node.y + 1));
                    setNodeParent(n, node);
                }
                if (map.getBlock(node.x - 1, node.y)) {
                    var n = new Node(node.x - 1, node.y, distance(xFinish, node.x - 1, yFinish, node.y));
                    setNodeParent(n, node);
                }
                if (map.getBlock(node.x, node.y - 1)) {
                    var n = new Node(node.x, node.y - 1, distance(xFinish, node.x, yFinish, node.y - 1));
                    setNodeParent(n, node);
                }
            }
            catch (IndexOutOfRangeException e) { }

            foreach (Node n in openList) {
                if (n.Equals(finishNode)) {
                    return n;
                }
            }
        }
        return null;
    }

    private void setNodeParent(Node n, Node node) {
        if (!closedList.Contains(n)) {
            if (!openList.Contains(n)) {
                openList.Add(n);
                n.setParent(node);
            }
            else {
                if (node.g < n.g) {
                    n.setParent(node);
                }
            }
        }
    }

    private int distance(int x1, int y1, int x2, int y2) {
        return Mathf.Abs(x1 - x2) + Mathf.Abs(y1 - y2);
    }

    public class Node {
        public int x { get; private set; }
        public int y { get; private set; }

        public int h { get; private set; }
        public int g { get; private set; }
        public int f { get; private set; }

        private Node parent;

        public Node GetParent() {
            return parent;
        }
        public void setParent(Node p) {
            parent = p;
            calculateG();
        }

        public Node(int x, int y, int h) {
            this.x = x;
            this.y = y;
            this.h = h;
            calculateG();
        }

        private void calculateG() {
            if (parent == null) {
                g = 0;
                f = 0;
                h = 0;
                return;
            }
            Node node = parent;
            g = 1;
            while (true) {
                if (node.parent != null) {
                    g += parent.g;
                    node = node.parent;
                }
                else {
                    break;
                }

            }

            f = h + g;
        }

        public override bool Equals(object obj) {
            Node other = (Node)obj;
            if (x == other.x && y == other.y) return true;
            else return false;
        }

        public override int GetHashCode() {
            return (x * 17) + (y * 7);
        }
    }
}
