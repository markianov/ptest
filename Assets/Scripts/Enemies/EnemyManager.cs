﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Класс EnemyManager - используется для отслеживания услоыий, при которых добавляются новые враги или меняется их поведение.
public class EnemyManager : MonoBehaviour {

    public static EnemyManager manager;


    public Enemy[] enemies;
    public Spawner spawner;
    
    void Start() {
        manager = this;
        spawner.zombieSpawn();
    }


    public void checkCoins() {
        int score = GameManager.getGameManager().getScore();
        if(score == 5) {
            spawner.zombieSpawn();
        }
        else if(score == 10) {
            spawner.mummySpawn();
        }
        else if(score == 20) {
            for(int i=0; i<3; i++) {
                enemies[i].startHunt();
            }
        }
        else if(score > 20) {
            for (int i = 0; i < 3; i++) {
                enemies[i].speed += enemies[i].speed * 0.05f;
            }
        }
    }
    
}
