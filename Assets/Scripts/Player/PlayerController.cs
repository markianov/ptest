﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float moveSpeed;
    public SpriteRenderer image;

    Transform player;
    bool lookRight = true;
    Animator animator;


    void Start() {
        animator = GetComponent<Animator>();
        player = transform;
        image = GetComponent<SpriteRenderer>();
    }

    void Update() {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        RaycastHit2D hit = Physics2D.Raycast(player.position, Vector2.right * h, 0.15f);

        if (hit.collider == null || hit.collider.tag != "Wall") {

            moveHorizontal(h);
            moveVertical(v);

            if (v == 0 && h == 0) {
                animator.SetBool("Walk", false);
            }
            else {
                animator.SetBool("Walk", true);
            }
        }
        else {
            animator.SetBool("Walk", false);
        }
    }

    private void moveHorizontal(float h) {
        player.Translate(Vector2.right * Time.deltaTime * moveSpeed * h);
        if (h > 0 && !lookRight) {
            image.flipX = false;
            lookRight = !lookRight;
        }
        else if (h < 0 && lookRight) {
            lookRight = !lookRight;
            image.flipX = true;
        }
    }

    private void moveVertical(float v) {
        player.Translate(Vector2.up * Time.deltaTime * moveSpeed * v);
    }
}
