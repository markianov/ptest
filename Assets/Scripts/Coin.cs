﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D col) {
        if (col.tag == "Player") {
            GameManager.getGameManager().addCoin();
            EnemyManager.manager.checkCoins();
            GameInterface.gi.updateInfo();

            gameObject.SetActive(false);
        }
    }
}
